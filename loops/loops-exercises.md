# Python Loops

Complete the following exercises using loops in Python

## Exercise Question 1: Print First 10 natural numbers using while loop

**Expected Output:**
```shell
0
1
2
3
4
5
6
7
8
9
10
```


## Exercise Question 2: Print the following pattern
**Expected Output:**
```shell
1 
1 2 
1 2 3 
1 2 3 4 
1 2 3 4 5
```

## Exercise Question 4: Print multiplication table of given number

For example ``` num=2```  so the output should be

**Expected Output**
```shell
2
4
6
8
10
12
14
16
18
20
```

## Exercise Question 5: Given a list iterate it and display numbers which are divisible by 5 and if you find number greater than 150 stop the loop iteration

```
list1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]
```

**Expected Output:**

```shell
15
55
75
150
```

## Exercise Question 6: Given a number count the total number of digits in a number

For example, the number is 75869, so the output should be 5.

## Exercise Question 7: Print the following pattern using for loop

```
5 4 3 2 1 
4 3 2 1 
3 2 1 
2 1 
1
```


## Exercise Question 8: Reverse the following list using for loop

```
list1 = [10, 20, 30, 40, 50]
```

**Expected Output:**

```shell
50
40
30
20
10
```

## Exercise Question 9: Display -10 to -1 using for loop

**Expected Output:**

```shell
-10
-9
-8
-7
-6
-5
-4
-3
-2
-1
```

## Exercise Question 10: Display a message “Done” after successful execution of for loop

For example, the following loop will execute without any error.
```python
for i in range(5):
    print(i)
```

**Expected output should be:**

```bash
0
1
2
3
4
Done!
```

## Exercise Question 11: Python program to display all the prime numbers within a range

Note: A Prime Number is a whole number that cannot be made by multiplying other whole numbers

**Examples:**
- 6 is not a Prime Number because it can be made by 2×3 = 6
- 37 is a Prime Number because no other whole numbers multiply together to make it.

**Given:**
```
start = 25
end = 50
```
**Expected Output:**
```bash
Prime numbers between 25 and 50 are:
29
31
37
41
43
47
```

## Exercise Question 12: Display Fibonacci series up to 10 terms

** Expected Output:**

```shell
Fibonacci sequence:
0  1  1  2  3  5  8  13  21  34
```

## Exercise Question 13: Write a loop to find the factorial of any number

The factorial (symbol: !) means to multiply all whole numbers from our chosen number down to 1.

**For example: calculate the factorial of 5**

```
5! = 5 × 4 × 3 × 2 × 1 = 120
```

**Expected Output:**

```bash
120
```

## Exercise Question 14: Reverse a given integer number

**Given:**

```
76542
```

**Expected output:**

```
24567
```

## Exercise Question 15: Use a loop to display elements from a given list which are present at even positions

**Given:**
```
my_list = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
```

**Expected output:**
```
20 40 60 80 100
```


